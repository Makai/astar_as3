package  
{
    import de.polygonal.ds.GraphNode;
    import de.polygonal.ds.Heapable;
    
    public class AStarWaypoint implements Heapable
    {
        /**
        * Cartesian coordinates (by default z=0).
        */
        public var x:Number = 0;
        public var y:Number = 0;
        public var z:Number = 0;
        
        /** Heapable interface */
        public var position:int = -1;
        
        /**
        * The total distance of all the edges that compromise the best path to this node so far.
        */
        public var distance:Number = NaN;
        
        /**
         * Heuristic estimate of the distance to the target to direct the search towards the target.
         */
        public var heuristic:Number = NaN;
        
        /**
         * True if this waypoint is contained in the queue.
         */
        public var onQue:Boolean = false;
        
        /**
         * The graph node that holds this waypoint.
         */
        public var node:GraphNode = null;
        
        public function AStarWaypoint() 
        {
            
        }
        
        public function reset():void
        {
            distance = 0;
            heuristic = 0;
            onQue = false;
        }
        
        public function distanceTo(wp:AStarWaypoint):Number
        {
            var dx:Number = wp.x - x;
            var dy:Number = wp.y - y;
            var dz:Number = wp.z - z;
            
            return Math.sqrt(dx * dx + dy * dy + dz * dz);
        }
        
        public function compare(other:Object):int
        {
            other = other as AStarWaypoint;
            if (other.heuristic > heuristic)
            {
                return 1;
            }
            else if (other.heuristic < heuristic)
            {
                return -1;
            }
            
            return 0;
        }
        
        public function toString():String
        {
            return ("AStarWaypoint: x=" + x + ", y=" + y);
        }
    }
}