package  
{
    import de.polygonal.ds._Heap.HeapElementWrapper;
    import de.polygonal.ds.DA;
    import de.polygonal.ds.Graph;
    import de.polygonal.ds.GraphArc;
    import de.polygonal.ds.GraphNode;
    import de.polygonal.ds.Heap;

    public class AStar 
    {
        protected var _que:Heap = new Heap();
        
        public function AStar() 
        {
        }
        
        public function free():void
        {
            _que.free();
            _que = null;
        }
        
        /**
         * Finds the shortest path from source to target and stores the result in path.
         * @return true if a path from source to target exists.
         */
        public function find(graph:Graph, source:AStarWaypoint, target:AStarWaypoint, path:DA):Boolean
        {
            var pathExists:Boolean = false;
            
            //reset search
            var walker:GraphNode = graph.getNodeList();
            while (walker != null)
            {
                //reset node & waypoint
                walker.marked = false;
                walker.parent = null;
                walker.val.reset();
                walker = walker.next;
            }
            
            //shortcut to _que
            var q:Heap = _que;
            
            //reset queue
            q.clear();
            
            //enqueue starting node
            q.add(source);
            
            //while there are waypoints in the queue...
            while (q.size() > 0)
            {
                //grab the next waypoint off the queue and process it
                var waypoint1:AStarWaypoint = q.pop() as AStarWaypoint;
                waypoint1.onQue = false;
                
                //each waypoint holds a reference to its GraphNode
                var node1:GraphNode = waypoint1.node;
                
                //make sure the waypoint wasn't visited before (can be visited multiple times)
                if (node1.marked) continue;
                
                //mark node as processed
                node1.marked = true;
                
                //exit if the target node has been found
                if (node1 == target.node)
                {
                    pathExists = true;
                    break;
                }
                
                //visit all connected nodes (denoted as waypoint2, node2)
                var arc:GraphArc = node1.arcList;
                while (arc != null)
                {
                    //the node our arc is pointing at
                    var node2:GraphNode = arc.node;
                    
                    //skip marked nodes
                    if (node2.marked)
                    {
                        arc = arc.next;
                        continue;
                    }
                    
                    var waypoint2:AStarWaypoint = node2.val as AStarWaypoint;
                    
                    //compute accumulated distance to get from the current waypoint (1) to the next waypoint (2)
                    var distance:Number = waypoint1.distance + waypoint1.distanceTo(waypoint2) * arc.cost;
                    
                    //node has been processed before ?
                    if (node2.parent != null)
                    {
                        //distance has been calculated before so check if new distance is shorter
                        if (distance < waypoint2.distance)
                        {
                            //switch to shorter path ('edge relaxation')
                            node2.parent = node1;
                            waypoint2.distance = distance;
                        }
                        else
                        {
                            //new distance > existing distance, skip
                            arc = arc.next;
                            continue;
                        }
                    }
                    else
                    {
                        //first time of being added to the queue - setup parent and distance
                        node2.parent = node1;
                        waypoint2.distance = distance;
                    }
                    
                    //compute A* heuristics
                    var heuristics:Number = waypoint2.distanceTo(target) + distance;
                    
                    //waypoints closest to the source node are processed first
                    waypoint2.heuristic = heuristics;
                    
                    //add to the search frontier
                    if (!waypoint2.onQue)
                    {
                        waypoint2.onQue = true;
                        q.add(waypoint2);
                    }
                    
                    arc = arc.next;
                }
            }
            
            if (pathExists)
            {
                //trace the path by working back through the parents
                //from the target node to the source node
                var starWalker:AStarWaypoint = target;
                while (starWalker != source)
                {
                    path.pushBack(starWalker);
                    starWalker = starWalker.node.parent.val as AStarWaypoint;
                }
                
                path.pushBack(source);
                path.reverse();
            }
            
            return pathExists;
        }
    }
}